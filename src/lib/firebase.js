// Import the functions you need from the SDKs you need
import { getApps, initializeApp, getApp, deleteApp} from "firebase/app";
import { getFirestore, addDoc, collection, serverTimestamp } from 'firebase/firestore';
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: import.meta.env.VITE_APIKEY,
    authDomain: import.meta.env.VITE_AUTH_DOMAIN,
    projectId: import.meta.env.VITE_PROJECT_ID,
    storageBucket: import.meta.env.VITE_STORAGE_BUCKET,
    messagingSenderId: import.meta.env.VITE_MESSAGE_SENDER_ID,
    appId: import.meta.env.VITE_APP_ID,
};

// Initialize Firebase

let firebaseApp;

if (!getApps().length) {
    firebaseApp = initializeApp(firebaseConfig);
} else {
    firebaseApp = getApp();
}

export const db = getFirestore(firebaseApp);
export const auth = getAuth(firebaseApp);
/**
 * @param {any} color
 */
export async function SendToFirebase(color) {
    let eventFb;
    try {
        const now = new Date();
     
        eventFb = {
            timestamp: serverTimestamp(),
            color

        }
        await addDoc(collection(db, `Which-Color`), eventFb);
        console.log("Event sent to firebase!");
    } catch (error) {
        console.log("Error while sending event:", eventFb, "to firebase: ",error);
    }
}